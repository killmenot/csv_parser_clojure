(ns myapp.routes.home
            (:require [myapp.layout :as layout]
                      [myapp.util :as util]
                      [clojure.string :as str]
                      [compojure.core :refer :all]
                      [org.httpkit.client :as http]
                      [noir.response :refer [edn]]
                      [clojure.pprint :refer [pprint]]))

(defn home-page []
  (layout/render "app.html"))

(defn get_table [doc]
  (let [{:keys [status headers body error] :as resp} @(http/get (doc :url))]
    (def body_parsed (str/split body #"\n"))
    (def rows (map (fn [record] (zipmap (map keyword (str/split (first body_parsed) #",")) (str/split record #","))) (rest body_parsed)))
  )
  rows)

(defroutes home-routes
  (GET "/" [] (home-page))
  (POST "/get_table" {:keys [body-params]}
    (edn (get_table body-params))))
